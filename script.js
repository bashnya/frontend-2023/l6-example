import { letterFrequencies } from './dataset.js';

// Сортируем данные
const data = letterFrequencies.sort((a, b) => b.frequency - a.frequency );

// Параметры графика
const HEIGHT = 300;
const WIDTH = 600;
const MARGIN = 20;

// Функция для получения значения, в зависимости от которого считать
// координату
const x = (dataElement) => dataElement.letter;
const y = (dataElement) => dataElement.frequency;

// Массивы значений, высчитанных функциями выше
const X = d3.map(data, x);
const Y = d3.map(data, y);

// Диапазон значений из массивов выше
const xDomain = X; // Тут строковые значения
const yDomain = [0, d3.max(Y)];

// Диапазон координат, в которых будет построен график
const xRange = [0, WIDTH];
const yRange = [HEIGHT, 0];

// Scale (масштаб) - определяется исходя из двух диапазонов
const xScale = d3.scaleBand(xDomain, xRange);
const yScale = d3.scaleLinear(yDomain, yRange);

// Оси
const xAxis = d3.axisBottom(xScale);
const yAxis = d3.axisLeft(yScale).ticks(HEIGHT / 20, '%');

// Добавляем холст
const canvas = d3.select('#canvas')
  .append('svg')
  .attr('width', WIDTH + MARGIN * 3)
  .attr('height', HEIGHT + MARGIN * 2);

// Добавляем оси
canvas.append('g')
  .attr('transform', `translate(${MARGIN * 2}, ${HEIGHT})`)
  .call(xAxis);

canvas.append('g')
  .attr('transform', `translate(${MARGIN * 2}, ${0})`)
  .call(yAxis);

// Добавляем прямоугольники (стобцы)
canvas.append('g')
  .attr('fill', 'aqua')
  .attr('transform', `translate(${MARGIN * 2}, ${0})`)
  .attr('stroke', 'black')
  .selectAll('rect')
  .data(data)
  .join('rect')
  .attr('x', el => xScale(x(el)))
  .attr('y', el => yScale(y(el)))
  .attr('width', xScale.bandwidth())
  .attr('height', el =>  yScale(0) - yScale(y(el)));
