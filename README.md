# БАШНЯ. Курс по фронту. Код к занятию 5


### На занятии мы разбирали

* Формат SVG
* Работу с библиотекой D3.js

### Полезные ссылки

- [В общем и целом про работу с SVG](https://ru.hexlet.io/blog/posts/kak-rabotat-s-formatom-svg-rukovodstvo-dlya-nachinayuschih-veb-razrabotchikov)
- [Референс SVG элементов в MDN](https://developer.mozilla.org/en-US/docs/Web/SVG/Element)
- [Входная точка в D3.js: их сайт с доками и примерами](https://d3js.org/)
- [Пошаговый гайд к D3.js](https://www.freecodecamp.org/news/d3js-tutorial-data-visualization-for-beginners/)
- [Много примеров того, что может D3.js](https://observablehq.com/@d3/gallery) &mdash; можно поиграться с кодом прямо там
